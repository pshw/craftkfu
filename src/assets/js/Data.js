/**
 * Function for get Url Response (Sync method)
 * @param {String} url Url for request
 * @returns response or null if url not responding
 */
function getUri(url) {
  var request = new XMLHttpRequest();
  request.open("GET", url, false);
  request.send();
  if (request.status == 200) return request.response;
  return null;
}

/**
 * Function for get Json Response (Sync method)
 * @param {String} url Url for json
 * @returns Json for Url or null if url not responding
 */
function getJson(url) {
  var rep = getUri(url);
  if (rep != null) rep = JSON.parse(rep);
  return rep;
}

let gitlabApiBasePath = "https://gitlab.com/api/v4";
let repositorySource = "18143513";
let targetFile = "building%2Fcraftkfu.json";
let targetVersionFile = "building%2Fversion.json";
let commits = getJson(
  gitlabApiBasePath +
    "/projects/" +
    repositorySource +
    "/repository/commits?path=" +
    targetFile
);
// Cf here : https://docs.gitlab.com/ee/api/commits.html#list-repository-commits
const version = commits[0]["id"];
let rawLocalData = localStorage.getItem("data");
let localData = rawLocalData != null ? JSON.parse(rawLocalData) : null;
let data = localData != null ? localData["data"] : null;
let gameDataVersion = localData != null ? localData["gameDataVersion"] : null;
let versionIsOutdated = localData == null || localData["version"] != version;
if (versionIsOutdated || gameDataVersion == null) {
  if (versionIsOutdated) {
    data = getJson(
      gitlabApiBasePath +
        "/projects/" +
        repositorySource +
        "/repository/files/" +
        targetFile +
        "/raw"
    );
  }
  if (gameDataVersion == null || versionIsOutdated) {
    gameDataVersion = getJson(
      gitlabApiBasePath +
        "/projects/" +
        repositorySource +
        "/repository/files/" +
        targetVersionFile +
        "/raw"
    )["version"];
  }
  localStorage.setItem(
    "data",
    JSON.stringify({
      version: version,
      data: data,
      gameDataVersion: gameDataVersion
    })
  );
}

export {
  version,
  data,
  gameDataVersion,
};
