import { data } from './Data.js';
import { Ingredient } from './Ingredient.js';
import { SimpleCraft } from './SimpleCraft.js';
import { defaultValues } from './Parse.js';
/* eslint-disable-next-line no-unused-vars */
import { Ref } from './Ref.js';
/* eslint-disable-next-line no-unused-vars */
import { Store } from './Store.js';
import { jobs } from './Localisation.js';


class CraftData {
    /**
     * Constructor of CraftData
     * @param {number} id id of this craft
     * @param {number} nb nb of this craft
     * @param {Array<String>} name name of this craft
     * @param {number} img gfxId of this craft
     * @param {number} rarity rarity of this craft
     * @param {number} xp xp given by this craft
     * @param {number} job job related to this craft
     * @param {number} lvl lvl of this craft
     * @param {number} lvlItem lvl of item related to this craft
     * @param {boolean} upgrade if craft is upgrade of previous item
     * @param {Ref} craftData data related to this craftData
     * @param {Array<SimpleCraft>} ingredients ingredient of this craft
     * @param {Array<SimpleCraft>} craftSpec crafts specification related to this craft
     * @param {boolean} considerAsIngredient Craft is processed like ingredient
     */
    constructor(id, nb, name, img, rarity, xp, job, lvl, lvlItem, upgrade, craftData, ingredients, craftSpec, considerAsIngredient) {
        this.id = id;
        this.nb = nb;
        this.name = name;
        this.img = img;
        this.rarity = rarity;
        this.xp = xp;
        this.job = job;
        this.lvl = lvl;
        this.lvlItem = lvlItem;
        this.upgrade = upgrade;
        this.craftData = craftData;
        this.ingredients = ingredients;
        this.craftSpec = craftSpec;
        this.considerAsIngredient = considerAsIngredient;
    }

    /**
     * Method for simple craft related to this craft
     * @returns {SimpleCraft} simpleCraft
     */
    getSimpleCraft() {
        return new SimpleCraft(this.id, this.qt, this.get, this.recipeSelected, this.considerAsIngredient);
    }
}

class Craft {

    /**
     * Constructor of Craft
     * @param {CraftData} craft data used for define this craft
     * @param {number} qt qt of this ingredient
     * @param {Array<SimpleCraft>} craftSpec crafts specification in this craft
     * @param {boolean} isHiding if this craft is hiding
     * @param {number} recipeSelected recipe selected for this craft
     * @param {boolean} considerAsIngredient craft are considered like ingredient
     */
    constructor(craft, qt = defaultValues.quantity, craftSpec = defaultValues.craftSpec,
                isHiding = defaultValues.hidden, recipeSelected = defaultValues.recipeSelected,
                considerAsIngredient = defaultValues.considerAsIngredient) {
        this.qtBase = Math.ceil(qt / craft.nb);
        this.qt = this.qtBase;
        this.id = craft.id;
        this.name = craft.name;
        this.img = craft.img;
        this.rarity = craft.rarity;
        this.xp = craft.xp;
        this.job = craft.job;
        this.lvl = craft.lvl;
        this.lvlItem = craft.lvlItem;
        this.nb = craft.nb;
        this.upgrade = craft.upgrade;
        this.craftData = craft.craftData;
        this.recipeSelected = recipeSelected;
        this.get = isHiding;
        this.ingredients = new Array(craft.ingredients.length);
        craft.ingredients.forEach((ings, i) => {
            this.ingredients[i] = [];
            ings.forEach((ing, k) => {
                let newIng = data.find(x => x.id === ing.id);
                let spec = craftSpec.find(y => y.id === ing.id);
                if (newIng.job !== 0) {
                    this.ingredients[i][k] = new Craft(
                        newIng, ing.qt * this.qt, spec != null ? spec.craftSpec : defaultValues.craftSpec,
                        spec != null ? spec.isHidden : defaultValues.hidden,
                        spec != null ? spec.recipeSelected : defaultValues.recipeSelected,
                        spec != null ? spec.considerAsIngredient : defaultValues.considerAsIngredient
                    );
                } else {
                    this.ingredients[i][k] = new Ingredient(newIng, ing.qt * this.qt, 0, spec != null ? spec.isHidden : defaultValues.hidden);
                }
            });
        });
        this.recipeSelected = this.recipeSelected < this.ingredients.length ? this.recipeSelected : defaultValues.recipeSelected;
        this.ingredients.forEach(recipe => recipe.forEach(component => component.registerOnChange(this._internalOnChange())));
        this._onChange = () => {};
        this.considerAsIngredient = considerAsIngredient;
    }

    /**
     * For get trigger on change if component are changed
     * @param {Craft} craft item handle call
     * @returns {(item: Craft|Ingredient) => void} functionInternal
     */
    /* eslint-disable-next-line no-unused-vars */
    _internalOnChange(craft = this) {
        return (item) => {
            if (item == craft)
                throw Error("Recursive call !");
            craft._onChange(craft);
        }
    }

    /**
     * For register method trigerred on each change
     * @param {(ingredient: Craft) => void} listener 
     */
    registerOnChange(listener) {
        this._onChange = listener;
    }

    /**
     * Method for simple craft related to this craft
     * @returns {SimpleCraft} simpleCraft
     */
    getSimpleCraft() {
        let spec = [];
        this.forEach(ing => {
            let craft = ing.getSimpleCraft();
            craft.qt = defaultValues.quantity;
            if (craft.recipeSelected != defaultValues.recipeSelected ||
                craft.isHidden != defaultValues.hidden ||
                craft.considerAsIngredient != defaultValues.considerAsIngredient ||
                craft.craftSpec.length > 0) {
                spec.push(craft);
            }
        });
        let out = new SimpleCraft(this.id, this.qt, this.get, this.recipeSelected, spec, this.considerAsIngredient);
        return out;
    }

    /**
     * Method for improve qt of this Craft
     * @param {number} qt qt added to this Craft
     */
    changeQt(qt) {
        this.qt = qt * Math.ceil(this.qtBase / this.nb);
        this.ingredients.forEach(mats => {
            mats.forEach(mat => {
                mat.qt = mat.qtBase * qt;
                if (mat.job !== 0) {
                    mat.registerOnChange(() => {});
                    mat.changeQt(qt);
                    mat.registerOnChange(this._internalOnChange());
                }
            });
        });
        this._onChange(this);
    }

    /**
     * Getter for get field
     * @returns {boolean} value of get
     */
    getGet() {
        return this.get;
    }

    /**
     * Method for change value of get
     */
    toggleGet() {
        this.setGet(!this.get);
    }

    /**
     * Setter for get field
     * @param {boolean} get new value for get
     */
    setGet(get) {
        this.get = get;
        this._onChange(this);
    }

    isCraft() {
        return !this.considerAsIngredient;
    }

    /**
     * Getter for considerAsIngredient field
     * @returns {boolean} value of considerAsIngredient
     */
    getConsiderAsIngredient() {
        return this.considerAsIngredient;
    }

    /**
     * Method for change value of considerAsIngredient
     */
    toggleConsiderAsIngredient() {
        this.setConsiderAsIngredient(!this.considerAsIngredient);
    }

    /**
     * Setter for considerAsIngredient field
     * @param {boolean} considerAsIngredient new value for considerAsIngredient
     */
    setConsiderAsIngredient(considerAsIngredient) {
        this.considerAsIngredient = considerAsIngredient;
        this._onChange(this);
    }

    /**
     * Method for change recipe used
     */
    switchRecipe() {
        if (this.ingredients.length > 1) {
            this.recipeSelected =
                this.recipeSelected + 1 === this.ingredients.length ? defaultValues.recipeSelected : this.recipeSelected + 1;
        }
        this._onChange(this);
    }

    /**
     * Method for get all Ingredients and put them inside array given
     * @param {Array<Ingredient|Craft>} arr array used
     */
    getMats(arr) {
        if (this.considerAsIngredient) {
            let check = arr.find(x => x.id === this.id);
            if (check === undefined)
                arr.push(new Craft(this, this.qt));
            else
                check.qt += this.qt;
        } else {
            this.forEach(mat => {
                if (mat.get === false)
                    mat.getMats(arr);
            });
        }
    }

    /**
     * Method for get step of this craft
     * @param {Store} store store used for localisation
     * @param {String} prefix prefix used for this step
     * @param {Dict} jobsData job data used for job localisation
     * @returns {String} stepOfThisCraft
     */
    getStep(store, prefix = '', jobsData = jobs) {
        let localised = prefix + this.getLocalisedString(store, jobsData);
        if (this.considerAsIngredient) {
            return localised;
        } else {
            let out = localised;
            this.ingredients[this.recipeSelected].forEach(ing => {
                if (ing.get === false)
                    out += '\n\t' + ing.getStep(store, prefix + '\t', jobsData);
            });
            return out;
        }
    }

    /**
     * Method for get localised string of this craft
     * @param {Store} store store used for localisation
     * @param {Dict} jobsData job data used for job localisation
     * @returns {String} stringLocalisedOfThisCraft
     */
    getLocalisedString(store, jobsData = jobs) {
        let out = store.getLocalisedString(this.name) + " x" + this.qt;
        if (this.job !== 0) {
            out += " (" + store.getLocalisedString(jobsData.find(x => x.id === this.job).name) + " Niv. " + this.lvl + ")";
        }
        return out;
    }

    /**
     * Apply callback in all ingredients of current recipe
     * @param {*} callbackfn callback applyed in ingredients
     */
    forEach(callbackfn) {
        this.ingredients[this.recipeSelected].forEach(callbackfn);
    }
}

export { Craft, CraftData };