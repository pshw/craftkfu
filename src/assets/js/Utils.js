/**
 * For wait for specific condition
 * @param {Function() : Boolean} conditionalFunction conditional function checked periodiquely
 * @param {Number} timeOut duration in ms until next check (300 ms by default)
 * @returns promise
 */
function waitForCondition(conditionalFunction, timeOut = 300) {
    return new Promise(resolve => {
        function checkCondition() {
            if (conditionalFunction()) {
                resolve();
            } else {
                window.setTimeout(checkCondition, timeOut);
            }
        }
        checkCondition();
  });
}

export {
    waitForCondition
};