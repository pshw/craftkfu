import { notify } from '@kyvg/vue3-notification';
import { List } from './List.js';
/* eslint-disable-next-line no-unused-vars */
import { Ref } from './Ref.js';
import { SaveInstruction } from './SaveInstruction.js';
import { allSeparators, defaultValues, extractEachArg, typeOf, typeExpected, typeExpectedShort } from './Parse.js';
/* eslint-disable-next-line no-unused-vars */
import { Store } from './Store.js';
import { SimpleCraft } from './SimpleCraft.js';
import { autoSaveValue, cardPrefix, containOnlyLocalCard, getCard } from './Card.js';
import { Craft, CraftData } from './Craft.js';
import { refs } from './RefsData.js';
import { identifiers, texts } from './Localisation.js';
import { Ingredient } from './Ingredient.js';

class DataStorage {

    /**
     * Constructor of DataStorage
     * @param {List} list list used for this dataStorage
     * @param {Array<Ref>} refs refs used for this dataStorage
     * @param {number} refsIdentified number of refs are identified
     * @param {boolean} refsInProcess ref are used by specific process
     * @param {String} lastCompressGen Type of last 'Compression'
     * @param {boolean} compressRefs Refs are compressed
     * @param {SaveInstruction} saveInstruction autoSave instruction if defined
     * @param {Store} storeRelated store related to this dataStorage
     */
    constructor(list = new List(), refs = [], refsIdentified = null, refsInProcess = false,
                lastCompressGen = null, compressRefs = null, saveInstruction = null, storeRelated = null) {
        this.list = list;
        this.refs = refs;
        this.refsIdentified = refsIdentified;
        this.refsInProcess = refsInProcess;
        this.lastCompressGen = lastCompressGen;
        this.compressRefs = compressRefs;
        this.saveInstruction = saveInstruction;
        this.storeRelated = storeRelated;
        if (this.storeRelated != null)
            this.triggerAutoSave();
    }

    /**
     * Setter of storeRelated
     * @param {Store} storeRelated store related to this dataStorage
     */
    setStoreRelated(storeRelated) {
        this.storeRelated = storeRelated;
        if (this.storeRelated != null)
            this.triggerAutoSave();
    }

    /**
     * For get Craft with id specified
     * @param {String} id Id of Craft
     * @returns {CraftData|IngredientData} craftFound
     */
    getCraft(id) {
        if (this.storeRelated == null) return null;
        const t = this.storeRelated.gameData.filter(x => x.id == id);
        return t[0] ? t[0] : null;
    }

    /**
     * For update recipes with recipe given
     * @param {SimpleCraft} recipe recipe used for add to dataStorage
     * @param {Number} iteration number of recipe are inserted
     * @param {CraftData|IngredientData} craft craft used for by pass check
     */
    updateRecipe(recipe, iteration = defaultValues.quantity, craft = null) {
        if (craft == null)
            craft = this.getCraft(recipe.id);
        const qt = recipe.qt * iteration; 
        if (craft != null)
            if (qt < 0)
                this.list.remove(craft, qt * -1);
            else if (qt > 0)
                this.list.add(craft, qt, recipe.craftSpec, recipe.isHidden, recipe.recipeSelected, recipe.considerAsIngredient);
    }

    /**
     * For update recipes with recipes given
     * @param {Array<SimpleCraft>} recipes recipes used for add to dataStorage
     * @param {Number} iteration number of recipe are inserted
     */
    updateRecipes(recipes, iteration = defaultValues.quantity) {
        recipes.forEach(recipe => this.updateRecipe(recipe, iteration));
    }

    /**
     * Function for get maxQuantity for recipe provided
     * @param {String} recipeId Id of recipe
     * @param {CraftData|IngredientData} craft craft used for by pass check
     * @returns {number} maxQuantity
     */
    maxQuantityOfRecipe(recipeId, craft = null) {
        if (craft == null) {
            craft = this.getCraft(recipeId);
        }
        craft = craft != null && craft.ingredients == null ? null : craft;
        return craft != null && !craft.get ? this.list.quantityOf(craft) : null;
    }

    /**
     * Function for get maxQuantity for all recipes provided
     * @param {Array<SimpleCraft>} recipes recipes used for add to dataStorage
     * @param {boolean} ignoreEmpty flag indicate if empty are ignored
     * @returns {number} maxQuantity
     */
    maxQuantityOfRecipes(recipes, ignoreEmpty = false) {
        let maxIteration = null;
        recipes.forEach(recipe => {
            let max = this.maxQuantityOfRecipe(recipe.id);
            if (!ignoreEmpty || max != 0) {
                let iteration = max / recipe.qt;
                if (max != null && (maxIteration == null || maxIteration > iteration))
                    maxIteration = iteration;
            }
        });
        return maxIteration;
    }

    /**
     * For create craft related to this ref
     * @param {Ref} refData data of ref
     * @returns {CraftData|IngredientData} recipeRelated
     */
    createRecipe(refData) {
        let recipesChecked = [];
        refData.recipes.forEach(recipe => {
            let craft = this.getCraft(recipe.id);
            if (craft != null)
                recipesChecked.push(recipe);
        });
        let beautifulName = refData.name[0].toUpperCase() + refData.name.slice(1, refData.name.length);
        let fullName = beautifulName + " - " + refData.code;
        return new CraftData(
            typeExpectedShort.distant + allSeparators.type + refData.name + allSeparators.type + refData.code,
            defaultValues.quantity, [fullName, fullName, fullName, fullName], refData.icon, 0, 0,
            -1, 0, 0, false, refData, [recipesChecked], refData.craftSpec, defaultValues.considerAsIngredient
        );
    }

    /**
     * For fetch recipe
     * @param {SimpleCraft} recipe recipe
     */
    fetchRecipe(recipe) {
        if (recipe != null) {
            if (recipe.id.length > 0) {
                let insideDataStorage = new DataStorage();
                insideDataStorage.setStoreRelated(new Store());
                insideDataStorage.storeRelated.gameData = this.storeRelated.gameData;
                recipe.craftSpec.forEach(inside => {
                    insideDataStorage.fetchRecipe(inside);
                });
                insideDataStorage.processRefs(true);
                let typeRelated = typeOf(recipe.id[0]);
                if (typeExpected.distant.indexOf(typeRelated) != -1) {
                    if (recipe.id.length >= 3) {
                        this.extractRecipesFromRef(recipe.id[1], recipe.id[2], recipe.qt,
                            recipe.isHidden, recipe.recipeSelected, insideDataStorage.list.getAllSimpleCraft(),
                            recipe.considerAsIngredient);
                        this.refsIdentified += 1;
                    } else {
                        console.error("Error on distant recipe " + recipe + " (missing data on id : " + recipe.id + ")");
                        notify({
                            title: this.storeRelated != null ?
                                    this.storeRelated.getLocalisedString(texts.notify.errors.wrongType.title) + ' : ' + recipe.getRawId() :
                                    "Error Occured",
                            text: this.storeRelated != null ?
                                    this.storeRelated.getLocalisedString(texts.notify.errors.wrongType.distant) :
                                    "Error Occured",
                            type: 'error'
                        });
                    }
                } else if (typeExpected.id.indexOf(typeRelated) != -1) {
                    let rawId = Number.parseInt(recipe.id[0]);
                    let id = Number.isInteger(rawId) ? rawId : Number.parseInt(recipe.id[1]);
                    if (Number.isInteger(id)) {
                        this.updateRecipe(
                            new SimpleCraft(
                                id, recipe.qt, recipe.isHidden, recipe.recipeSelected,
                                insideDataStorage.list.getAllSimpleCraft(),
                                recipe.considerAsIngredient
                            )
                        );
                    } else {
                        console.error("Error on recipe id" + recipe + " (id must be a integer : " + recipe.id + ")");
                        notify({
                            title: this.storeRelated != null ?
                                this.storeRelated.getLocalisedString(texts.notify.errors.wrongType.title) + ' : ' + recipe.getRawId() :
                                "Error Occured",
                            text: this.storeRelated != null ?
                                this.storeRelated.getLocalisedString(texts.notify.errors.wrongType.id) :
                                "Error Occured",
                            type: 'error'
                        });
                    }
                } else if (typeExpected.local.indexOf(typeRelated) != -1) {
                    if (recipe.id.length >= 2) {
                        let card = getCard(recipe.id[1])
                        if (card != null)
                            card.forEach(data => {
                                data.qt = data.qt * recipe.qt;
                                this.fetchRecipe(data);
                            });
                        else {
                            console.error("Error on local card " + recipe + " (data not found with id : " + recipe.id + ")");
                            notify({
                                title: this.storeRelated != null ?
                                    this.storeRelated.getLocalisedString(texts.notify.errors.defaultTitle) + ' : ' + recipe.getRawId() :
                                    "Error Occured",
                                text: this.storeRelated != null ?
                                    this.storeRelated.getLocalisedString(texts.notify.errors.localCardNotFound).replace(identifiers.card, recipe.id[1]) :
                                    "Error Occured",
                                type: 'error'
                            });
                        }
                    } else {
                        console.error("Error on local card " + recipe + " (missing data on id : " + recipe.id + ")");
                        notify({
                            title: this.storeRelated != null ?
                                this.storeRelated.getLocalisedString(texts.notify.errors.wrongType.title) + ' : ' + recipe.getRawId() :
                                "Error Occured",
                            text: this.storeRelated != null ?
                                this.storeRelated.getLocalisedString(texts.notify.errors.wrongType.local) :
                                "Error Occured",
                            type: 'error'
                        });
                    }
                } else if (typeExpected.encoded.indexOf(typeRelated) != -1) {
                    if (recipe.id.length >= 2) {
                        let encoded = atob(recipe.id[1]);
                        this.fetchRecipes(encoded);
                    } else {
                        console.error("Error on encoded data " + recipe + " (Unable to retrieve data from encoded date : " + recipe.id + ")");
                        notify({
                            title: this.storeRelated != null ?
                                this.storeRelated.getLocalisedString(texts.notify.errors.defaultTitle) + ' : ' + recipe.getRawId() :
                                "Error Occured",
                            text: this.storeRelated != null ?
                                this.storeRelated.getLocalisedString(texts.notify.errors.encodedDataMissing).replace(identifiers.encodedData, recipe.id[1]) :
                                "Error Occured",
                            type: 'error'
                        });
                    }
                } else {
                    console.error("Error on fetch recipe " + recipe + " (id is incorrect : " + recipe.id + ")");
                    notify({
                        title: this.storeRelated != null ?
                            this.storeRelated.getLocalisedString(texts.notify.errors.wrongType.title) + ' : ' + recipe.getRawId() :
                            "Error Occured",
                        text: this.storeRelated != null ?
                            this.storeRelated.getLocalisedString(texts.notify.errors.wrongType.unknown) :
                            "Error Occured",
                        type: 'error'
                    });
                }
            }
        }
    }

    /**
     * For fetch all recipes provided
     * @param {String} argsProvided args used
     */
    fetchRecipes(argsProvided) {
        let args = extractEachArg(argsProvided);
        args.forEach(recipe => this.fetchRecipe(recipe));
        this.processRefs(true);
        if (containOnlyLocalCard(args))
            this.enableAutoSave(args[0].id[1]);
        else
            this.enableAutoSave(autoSaveValue, true, '');
    }

    /**
     * For extract recipes from specific ref
     * @param {String} refName Name of Ref
     * @param {String} code Code of this Ref
     * @param {number} iteration Iteration of this Ref
     * @param {boolean} isHidden Craft is Hidden
     * @param {number} recipeSelected recipeSelected
     * @param {Array<SimpleCraft>} craftSpec Spec of components of this Ref
     * @param {boolean} considerAsIngredient Craft is processed like ingredient
     */
    extractRecipesFromRef(refName, code, iteration, isHidden, recipeSelected, craftSpec, considerAsIngredient) {
        /**
         * Handle of Error
         * @param {Error | String} error 
         */
        let handleError = (error) => {
            console.error("Error on fetch ref " + code + " in " + refName + " with cause : " +
                typeof error == 'Error' ? error.message : error);
            notify({
                title: this.storeRelated != null ?
                    this.storeRelated.getLocalisedString(texts.notify.errors.defaultTitle) :
                    "Error Occured",
                text: this.storeRelated != null ?
                    this.storeRelated.getLocalisedString(texts.notify.errors.distant).replace(identifiers.provider, refName)
                        .replace(identifiers.error, error instanceof Error ? error.message : error)
                    : "Error occured",
                type: 'error'
            });
            this.refsIdentified -= 1;
            this.processRefs(true);
        };
        if (refs[refName.toLowerCase()] != null) {
            let ref = refs[refName.toLowerCase()];
            fetch(
                ref.apiEndpoint(code),
                ref.apiRequest(code)
            ).then(res => {
                if (res.ok)
                    res.json().then(json => {
                        let recipes = null;
                        try {
                            recipes = ref.extract(json);
                        } catch (error) {
                            handleError(error)
                        }
                        if (recipes) {
                            let buildData = new Ref(
                                refName, code, ref.link(code),
                                ref.icon, iteration, recipes, craftSpec, isHidden,
                                recipeSelected, considerAsIngredient
                            );
                            this.addBuild(buildData);
                        }
                    }, error => {
                        handleError(error);
                    });
                else
                    handleError(res.statusText);
            }, error => {
                handleError(error);
            });
        }
    }

    /**
     * For add build to dataStorage
     * @param {Ref} buildData Data related to this build
     */
    addBuild(buildData) {
        buildData.craftData = this.createRecipe(buildData);
        this.refs.push(buildData);
        this.processRefs(true);
    }

    /**
     * For display refs with specific rendered
     * @param {Boolean} withoutDeletion For prevent deletion in some case
     */
    processRefs(withoutDeletion = false) {
        if (this.refsIdentified != null && this.refs.length == this.refsIdentified &&
            this.refInProcess == false) {
            this.refInProcess = true;
            if (this.lastCompressGen != null && !withoutDeletion) {
                if (this.lastCompressGen == "compressed")
                    this.removeCompressedRefs();
                else
                    this.removeUnCompressRefs();
            }
            if (this.compressRefs == null)
                this.compressRefs = true;
            if (!this.compressRefs)
                this.addUnCompressRefs();
            else
                this.addCompressedRefs();
            this.lastCompressGen = this.compressRefs ? "compressed" : "expanded";
            this.refInProcess = false;
        }
    }

    /**
     * For remove compressed refs
     */
    removeCompressedRefs() {
        this.refs.forEach(ref => {
            ref.iteration = this.maxQuantityOfRecipe(ref.id, ref.craftData);
            ref.currentCraft = this.list._find(ref.craftData);
            ref.craftSpec = [];
            ref.currentCraft.ingredients[ref.currentCraft.recipeSelected].forEach(ing => {
                if (ing instanceof Craft) {
                    let rawCraft = ing.getSimpleCraft();
                    rawCraft.qt = defaultValues.quantity;
                    ref.craftSpec.push(rawCraft);
                }
            });
            this.updateRecipe(new SimpleCraft(ref.id, -1), ref.iteration, ref.craftData);
        });
    }
    /**
     * For add compressed refs
     */
    addCompressedRefs() {
        this.refs.forEach(ref => {
            this.updateRecipe(
                new SimpleCraft(ref.id, 1, ref.isHidden, ref.recipeSelected, ref.craftSpec, ref.considerAsIngredient),
                ref.iteration, ref.craftData);
            ref.currentCraft = this.list._find(ref.craftData);
        });
    }
    /**
     * For remove uncompressed refs
     */
    removeUnCompressRefs() {
        this.refs.forEach(ref => {
            ref.iteration = this.maxQuantityOfRecipes(ref.recipes, true);
            ref.craftSpec = [];
            ref.recipes.forEach(recipe => {
                let data = this.getCraft(recipe.id);
                if (data != null) {
                    let entity = this.list._find(data);
                    if (entity != null) {
                        ref.craftSpec.push(new SimpleCraft(entity.id, defaultValues.quantity,
                            false, entity.recipeSelected, entity.getSimpleCraft().craftSpec,
                            defaultValues.considerAsIngredient));
                    } else {
                        ref.craftSpec.push(new SimpleCraft(data.id, defaultValues.quantity,
                            true, defaultValues.recipeSelected, defaultValues.craftSpec,
                            defaultValues.considerAsIngredient));
                    }
                }
            });
            this.updateRecipes(ref.recipes, (ref.iteration * -1));
        });
    }
    /**
     * For add uncompressed refs
     */
    addUnCompressRefs() {
        this.refs.forEach(ref => {
            ref.recipes.forEach(recipe => {
                let recipeUsed = ref.craftSpec.find(x => x.id == recipe.id);
                if (recipeUsed && !recipeUsed.isHidden)
                    this.updateRecipe(recipeUsed, ref.iteration);
            });
        })
    }

    /**
     * For trigger auto save if auto save are enabled
     */
    triggerAutoSave() {
        if (this.saveInstruction != null && this.saveInstruction.autoSave) {
            this.list.registerOnChange((list) => {
                if (this.saveInstruction == null || !this.saveInstruction.autoSave) {
                    this.list.registerOnChange(() => { });
                    return;
                };

                let error = list.save(this.saveInstruction.name, this.saveInstruction.prefix);
                if (!this.saveInstruction.hideLog) {
                    if (error)
                        notify({
                            title: this.storeRelated != null ?
                                this.storeRelated.getLocalisedString(texts.notify.errors.save.title) :
                                "Error Occured",
                            text: this.storeRelated != null ?
                                this.storeRelated.getLocalisedString(texts.notify.errors.save.saveFailedAndWhy) :
                                "Error Occured",
                            type: 'error'
                        });
                    else
                        notify(
                            this.storeRelated != null ?
                                this.storeRelated.getLocalisedString(texts.notify.card.saved) :
                                "Error Occured"
                        );
                }
            });
        } else
            this.list.registerOnChange(() => {});
    }

    /**
     * For enable autoSave for specific card
     * @param {String} name name of card
     * @param {boolean} hideLog if log are hidden
     * @param {String} prefix prefix for card
     */
    enableAutoSave(name, hideLog = false, prefix = cardPrefix) {
        this.saveInstruction = new SaveInstruction(name, true, hideLog, prefix);
        this.triggerAutoSave();
    }

    /**
     * For disable autoSave for specific card
     * @param {String} name name of card
     * @param {boolean} hideLog if log are hidden
     * @param {String} prefix prefix for card
     */
    disableAutoSave(name = null, hideLog = false, prefix = cardPrefix) {
        this.saveInstruction = name != null ? new SaveInstruction(name, false, hideLog, prefix) : null;
        this.triggerAutoSave();
    }

    /**
     * For clear this DataStorage
     */
    clear() {
        this.list.clear();
        this.refs = [];
        this.refsIdentified = null;
        this.refInProcess = false;
        this.lastCompressGen = null;
        this.compressRefs = null;
        this.saveInstruction = null;
    }

    /**
     * To remove ingredient provided
     * @param {Ingredient|Craft} data ingredient/craft removed
     */
    removeIngredient(data) {
        console.log(data);
        let fun = (c) => {
            if (c instanceof Craft) {
                if (c.getConsiderAsIngredient() && c.id === data.id && c.getGet() === false) {
                  c.setGet(true);
                } else {
                  c.forEach(fun);
                }
            } else if (c instanceof Ingredient && c.id === data.id && c.getGet() === false) {
                c.setGet(true);
            }
        };
        this.list.forEach(fun);
    }
}

export { DataStorage };