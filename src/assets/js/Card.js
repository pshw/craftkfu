/* eslint-disable-next-line no-unused-vars */
import { SimpleCraft } from "./SimpleCraft.js";
/* eslint-disable-next-line no-unused-vars */
import { List } from "./List.js";
import {
  typeExpected, defaultValues,
  typeOf,
  extractEachArg,
  argsForArrayOfSimpleCraft,
  containSeparator,
} from "./Parse.js";


/**
 * For know if arg is only a blank existing local card
 * @param {Array<SimpleCraft>} args currentArg
 * @returns containOnlyLocalCard
 */
function containOnlyLocalCard(args) {
    return (
      args.length == 1 &&
      typeExpected.local.indexOf(typeOf(args[0].id[0])) != -1 &&
      args[0].id.length >= 2 &&
      args[0].qt == defaultValues.quantity &&
      args[0].craftSpec.length == defaultValues.craftSpec.length &&
      args[0].recipeSelected == defaultValues.recipeSelected &&
      args[0].isHidden == defaultValues.hidden &&
      getCardsAvailable().findIndex((item) => item == args[0].id[1]) != -1
    );
}

const cardPrefix = "Card/";
const autoSaveValue = "AutoSave/Card";

/**
 * For store build inside localStorage
 * @param {String} name name of card
 * @param {List} cardData data of card
 * @param {String} prefix prefix for card
 * @throws {QuotaExceededError} if storage is disable or full for this site
 */
function saveCard(name, cardData, prefix = cardPrefix) {
    if (containSeparator(name))
        throw Error("Name must not contain separator");
    localStorage.setItem(prefix + name, argsForArrayOfSimpleCraft(cardData.getAllSimpleCraft()));
}

/**
 * For get specific card inside localStorage
 * @param {String} name name of card
 * @param {String} prefix prefix for card
 * @returns {Array<SimpleCraft>} cardRelated
 */
function getCard(name, prefix = cardPrefix) {
    let data = localStorage.getItem(prefix + name);
    return data != null ? extractEachArg(data) : null;
}

/**
 * For get arg related to specific card
 * @param {String} name name of card
 * @param {String} prefix prefix for card
 * @returns {String} argOfCard
 */
function getArgOfCard(name, prefix = cardPrefix) {
    let data = getCard(name, prefix);
    return data != null ? argsForArrayOfSimpleCraft(data) : null;
}

/**
 * For remove specific card inside localStorage
 * @param {String} prefix prefix for card
 * @param {String} name name of card
 */
function removeCard(name, prefix = cardPrefix) {
    localStorage.removeItem(prefix + name);
}

/**
 * For get name of Cards Available
 * @param {String} prefix prefix for cards
 * @returns {Array<String>} nameOfCardsAvailable
 */
function getCardsAvailable(prefix = cardPrefix) {
    let keysAvailable = Object.keys(localStorage);
    let cards = [];
    keysAvailable.forEach(key => {
        if (key.startsWith(prefix))
            cards.push(key.replace(prefix, ''));
    });
    return cards;
}

export {
    autoSaveValue,
    cardPrefix,
    containOnlyLocalCard,
    saveCard,
    getCard,
    getArgOfCard,
    removeCard,
    getCardsAvailable
}