import { jobs } from "./Localisation.js";
import { defaultValues } from "./Parse.js";
import { SimpleCraft } from "./SimpleCraft.js";
/* eslint-disable-next-line no-unused-vars */
import { Store } from './Store.js';

class IngredientData {

    /**
     * Constructor of IngredientData
     * @param {number} id id of this craft
     * @param {Array<String>} name name of this craft
     * @param {number} img gfxId of this craft
     * @param {number} rarity rarity of this craft
     * @param {number} type type of this craft
     */
    constructor(id, name, img, rarity, type) {
        this.id = id;
        this.name = name;
        this.img = img;
        this.rarity = rarity;
        this.type = type;
    }
}

class Ingredient {

    /**
     * Constructor of Ingredient
     * @param {IngredientData} ingredient data used for define this ingredient
     * @param {number} qt qt of this ingredient
     * @param {number} job job related to this ingredient
     * @param {boolean} isHiding if this craft is hiding
     */
    constructor(ingredient, qt, job = 0, isHiding = defaultValues.hidden) {
        this.qtBase = qt;
        this.qt = qt;
        this.id = ingredient.id;
        this.name = ingredient.name;
        this.img = ingredient.img;
        this.rarity = ingredient.rarity;
        this.type = ingredient.type;
        this.job = job;
        this.get = isHiding;
        this._onChange = () => {};
    }

    /**
     * For register method trigerred on each change
     * @param {(ingredient: Ingredient) => void} listener 
     */
    registerOnChange(listener) {
        this._onChange = listener;
    }

    /**
     * Method for improve qt of this Ingredient
     * @param {number} qt qt added to this ingredient
     */
    addQt(qt) {
        this.qt += qt;
        this._onChange(this);
    }

    /**
     * Getter for get field
     * @returns {boolean} value of get
     */
    getGet() {
        return this.get;
    }

    /**
     * Method for change value of get
     */
    toggleGet() {
        this.setGet(!this.get);
    }

    /**
     * Setter for get field
     * @param {boolean} get new value for get
     */
    setGet(get) {
        this.get = get;
        this._onChange(this);
    }

    isCraft() {
        return false;
    }

    /**
     * Method for improve qt of this Ingredient
     * @param {number} qt qt added to this Craft
     */
    changeQt(qt) {
        this.qt = qt;
        this._onChange(this);
    }

    /**
     * Method for simple craft related to this Ingredient
     * @returns {SimpleCraft} simpleCraft
     */
    getSimpleCraft() {
        return new SimpleCraft(this.id, defaultValues.quantity, this.get);
    }

    /**
     * Method for get ingredients and put them inside array given
     * @param {Array<Ingredient|Craft>} arr array used
     */
    getMats(arr) {
        if (this.get === false) {
            let check = arr.find(x => x.id === this.id);
            if (check === undefined)
                arr.push(new Ingredient(this, this.qt));
            else
                check.qt += this.qt;
        }
    }

    /**
     * Method for get step of this ingredient
     * @param {Store} store store used for localisation
     * @param {String} prefix prefix used for this step
     * @param {Dict} jobsData job data used for job localisation
     * @returns {String} stepOfThisIngredient
     */
    getStep(store, prefix = '', jobsData = jobs) {
        return prefix + this.getLocalisedString(store, jobsData);
    }

    /**
     * Method for get localised string of this ingredient
     * @param {Store} store store used for localisation
     * @param {Dict} jobsData job data used for job localisation
     * @returns {String} stringLocalisedOfThisIngredient
     */
    getLocalisedString(store, jobsData = jobs) {
        let out = store.getLocalisedString(this.name) + " x" + this.qt;
        if (this.job != 0) {
            out += " (" + store.getLocalisedString(jobsData.find(x => x.id === this.job).name) + " Niv." + this.lvl + ")";
        }
        return out;
    }
}

export { Ingredient, IngredientData };