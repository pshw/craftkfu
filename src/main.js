import { createApp } from 'vue';
import App from './App.vue';

import VTooltip from 'v-tooltip';
import 'v-tooltip/dist/v-tooltip.css';

import VueUniversalModal from 'vue-universal-modal';
import 'vue-universal-modal/dist/index.css';

import { FontAwesomeIcon } from './config/fontAwesome';

import Notifications from '@kyvg/vue3-notification'

import './assets/scss/variables.scss';
import './assets/scss/modal.scss';
import './assets/css/switch.css';

let app = createApp(App);

app.use(VTooltip);

app.use(VueUniversalModal, {
    teleportTarget: '#modals'
});

app.component('font-awesome-icon', FontAwesomeIcon);

app.use(Notifications);

app.mount('#app');
