import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faExpandArrowsAlt,
    faCompressArrowsAlt,
    faChevronDown,
    faChevronUp,
    faEraser,
    faSync,
    faFileExcel,
    faFileAlt,
    faShare,
    faSave,
    faArrowCircleDown,
    faArrowCircleUp,
    faArrowUp,
    faSwatchbook,
    faLink,
    faCheckCircle,
    faTimesCircle,
    faThumbsDown,
    faThumbsUp,
    faChevronCircleDown,
    faCartArrowDown,
    faTimes,
    faExchangeAlt,
    faCopy,
} from '@fortawesome/free-solid-svg-icons';
import { faGitlab, faDiscord } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(
    faGitlab,
    faDiscord,
    faExpandArrowsAlt,
    faCompressArrowsAlt,
    faChevronUp,
    faChevronDown,
    faEraser,
    faSync,
    faFileExcel,
    faFileAlt,
    faShare,
    faSave,
    faArrowCircleDown,
    faArrowCircleUp,
    faArrowUp,
    faSwatchbook,
    faLink,
    faCheckCircle,
    faTimesCircle,
    faThumbsDown,
    faThumbsUp,
    faChevronCircleDown,
    faCartArrowDown,
    faTimes,
    faExchangeAlt,
    faCopy,
);

export {
    FontAwesomeIcon
};